# Changelog - Bepic Core module
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Released]

## [1.0.0] - 27-04-2020
### Added
- Base support for menu.xml.
- Link from menu to configuration section.
- Base support for configuration in system.xml.
- Base configuration helper object.
- ACL records to give access rights to Module and configuration.
- README and VERSION files