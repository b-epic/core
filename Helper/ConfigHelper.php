<?php

namespace Bepic\Core\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Config helper class for easy access to config values
 * @package Helper
 */
class ConfigHelper extends AbstractHelper
{
    // Main path for BEpic configurations
    public const XML_PATH_BEPIC = 'bepic/';
    // Path for all general config in Bepic configurations
    public const XML_PATH_GENERAL = 'general/';

    /**
     * Get a config value from the config settings
     *
     * @param string $field
     * @param string|int $storeId
     * @return mixed
     */
    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    /**
     * Get config settings from the general config group in the bepic configurations
     *
     * @param string $code
     * @param string|int $storeId
     * @return mixed
     */
    public function getGeneralConfig($code, $storeId = null)
    {
        return $this->getConfigValue(
            self::XML_PATH_BEPIC . self::XML_PATH_GENERAL . $code, $storeId
        );
    }

    /**
     * Check if the Bepic module is enabled
     *
     * @param string|int $storeId
     * @return boolean
     */
    public function isModuleEnabled($storeId = null): bool
    {
        return ($this->getGeneralConfig('enabled', $storeId) === "1");
    }
}