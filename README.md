# README
Installation steps

## Installation through composer (recommended)
Run the following command to add the bepic repo to your composer.json file (if not already present)
composer config repositories.bepic vcs git@bitbucket.org:b-epic/core.git
Then run the command
composer require bepic/magento-2-core-extension:dev-master
Run bin/magento setup:upgrade command
your module is now installed

## Installation manually
Download the source code and add them into the folder:
app/code/Bepic/Core
The folder structure is CASE SENSITIVE